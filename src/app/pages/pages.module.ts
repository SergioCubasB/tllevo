import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { AppRoutingModule } from '../app-routing.module';
import { HomeComponent } from './home/home.component';

import { SharedModule } from '../shared/shared.module';
import { MaterialModule } from '../shared/material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SupportComponent } from './support/support.component';
import { SearchSupportComponent } from './support/search-support/search-support.component';
import { DocumentationComponent } from './documentation/documentation.component';
import { HelpComponent } from './help/help.component';
import { DetailHelpComponent } from './help/detail-help/detail-help.component';
import { DataHelpComponent } from './help/data-help/data-help.component';
import { UsersComponent } from './users/users.component';
import { DataSupportComponent } from './support/data-support/data-support.component';
import { NgChartsModule } from 'ng2-charts';

@NgModule({
  declarations: [

    HomeComponent,
    SupportComponent,
    SearchSupportComponent,
    DataSupportComponent,
    DocumentationComponent,
    HelpComponent,
    DataHelpComponent,
    DetailHelpComponent,
    UsersComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgChartsModule,

    AppRoutingModule,
    SharedModule
  ]
})
export class PagesModule { }
