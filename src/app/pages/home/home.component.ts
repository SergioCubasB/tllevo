import { Component, OnInit, ViewChild } from '@angular/core';

import { ChartConfiguration, ChartData, ChartEvent, ChartType } from 'chart.js';
import { BaseChartDirective } from 'ng2-charts';

import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';

import {MatDialog} from '@angular/material/dialog';
import { DetailComponent } from 'src/app/shared/component/modals/detail/detail.component';
import { ChangeStateComponent } from 'src/app/shared/component/modals/change-state/change-state.component';

/* Import Data for test */
import client from '../../data/client.json';
import tickets from '../../data/tickets.json';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  @ViewChild(BaseChartDirective) chart: BaseChartDirective | undefined;

  public barChartOptions: ChartConfiguration['options'] = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: {
      x: {},
      y: {
        min: 10
      }
    },
    backgroundColor: "#FF5A56",
    plugins: {
      legend: {
        display: false,
      }
    }
  };
  public barChartType: ChartType = 'bar';


  public barChartData: ChartData<'bar'> = {
    labels: [ 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    datasets: [
      { data: [ 65, 59, 35, 81, 56, 55, 40 ], label: 'Venta total' }
    ]
  };



  // Doughnut
  public barDoughnutOptions: ChartConfiguration['options'] = {
    responsive: true,
    backgroundColor: "#5dc728",
  };


  public doughnutChartLabels: string[] = [];
  public doughnutChartData: ChartData<'doughnut'> = {
    labels: this.doughnutChartLabels,
    datasets: [
      { data: [ 350, 450, 100 ] }
    ]
  };
  public doughnutChartType: ChartType = 'doughnut';

  // events
  public chartClicked({ event, active }: { event?: ChartEvent, active?: {}[] }): void {
    //console.log(event, active);
  }

  public chartHovered({ event, active }: { event?: ChartEvent, active?: {}[] }): void {
    //console.log(event, active);
  }

  public randomize(): void {
    // Only Change 3 values
    this.barChartData.datasets[0].data = [
      Math.round(Math.random() * 100),
      59,
      80,
      Math.round(Math.random() * 100),
      56,
      Math.round(Math.random() * 100),
      40 ];

    this.chart?.update();
  }

  /* Datatable  */
  constructor(
    public dialog: MatDialog
  ) {

   }

  ngOnInit(): void {

    console.log(client);
    console.log(tickets);
  }


  /* Datatable */
  displayedColumns: string[] = ['id', 'estado', 'vendedor', 'fecha', 'precio','accion'];
  displayedColumnsPendientes: string[] = ['id', 'idUsuario', 'fecha','accion'];

  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  dataSourcePendientes = new MatTableDataSource<Pendientes>(ELEMENT_DATA_Pendientes);


  @ViewChild(MatPaginator) paginator:any = MatPaginator;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSourcePendientes.paginator = this.paginator;

  }



  openDialog(){
    this.dialog.open(DetailComponent, {
      width: '800px',
      panelClass: 'detailModal',
      data: {
        title: 'Estado del Envio',
      },
    });
  }

  openDialogChangeState(){
    this.dialog.open(ChangeStateComponent, {
      width: '500px',
      panelClass: 'detailModal',
      data: {
        title: 'Cambio de estado de producto',
      },
    });
  }


}

export interface PeriodicElement {
  id: string;
  estado: string;
  vendedor: string;
  fecha: string;
  precio: string;
  accion: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {id: "#09123123", estado: 'ENTREGADO', vendedor: "Carlos Alcantara", fecha: '05/12/2021', precio: '$3799.00', accion: ''},
  {id: "#09123123", estado: 'ENTREGADO', vendedor: "Carlos Alcantara", fecha: '05/12/2021', precio: '$3799.00', accion: ''},
  {id: "#09123123", estado: 'ENTREGADO', vendedor: "Carlos Alcantara", fecha: '05/12/2021', precio: '$3799.00', accion: ''},
  {id: "#09123123", estado: 'ENTREGADO', vendedor: "Carlos Alcantara", fecha: '05/12/2021', precio: '$3799.00', accion: ''},
  {id: "#09123123", estado: 'ENTREGADO', vendedor: "Carlos Alcantara", fecha: '05/12/2021', precio: '$3799.00', accion: ''},
  {id: "#09123123", estado: 'ENTREGADO', vendedor: "Carlos Alcantara", fecha: '05/12/2021', precio: '$3799.00', accion: ''},
  {id: "#09123123", estado: 'ENTREGADO', vendedor: "Carlos Alcantara", fecha: '05/12/2021', precio: '$3799.00', accion: ''},
  {id: "#09123123", estado: 'ENTREGADO', vendedor: "Carlos Alcantara", fecha: '05/12/2021', precio: '$3799.00', accion: ''},
  {id: "#09123123", estado: 'ENTREGADO', vendedor: "Carlos Alcantara", fecha: '05/12/2021', precio: '$3799.00', accion: ''},
];



export interface Pendientes {
  id: string;
  idUsuario: string;
  fecha: string;
  accion: string;
}

const ELEMENT_DATA_Pendientes: Pendientes[] = [
  {id: "#321531235324", idUsuario: 'U-12312312312', fecha: "05/12/2021",  accion: ''},
  {id: "#321531235324", idUsuario: 'U-12312312312', fecha: "05/12/2021",  accion: ''},
  {id: "#321531235324", idUsuario: 'U-12312312312', fecha: "05/12/2021",  accion: ''},
  {id: "#321531235324", idUsuario: 'U-12312312312', fecha: "05/12/2021",  accion: ''},
  {id: "#321531235324", idUsuario: 'U-12312312312', fecha: "05/12/2021",  accion: ''},
  {id: "#321531235324", idUsuario: 'U-12312312312', fecha: "05/12/2021",  accion: ''}
];