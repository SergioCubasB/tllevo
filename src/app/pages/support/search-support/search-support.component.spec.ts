import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchSupportComponent } from './search-support.component';

describe('SearchSupportComponent', () => {
  let component: SearchSupportComponent;
  let fixture: ComponentFixture<SearchSupportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchSupportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchSupportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
