import {SelectionModel} from '@angular/cdk/collections';

import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import {MatTableDataSource} from '@angular/material/table';

export interface PeriodicElement {
  idTicket: string;
  idUsuario: string;
  categoria: string;
  fecha: string;
  verTicket: string;
  estado: string;
  accion: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {idTicket: '321531235324', idUsuario: 'U-123125812', categoria: 'DENUNCIA - PEDIDO', fecha: '24 Febrero', verTicket: 'true', estado:'Resuelto', accion:''},
  {idTicket: '321531235324', idUsuario: 'U-123125812', categoria: 'DENUNCIA - PRODUCTO', fecha: '22 Febrero', verTicket: 'true', estado:'Pendiente', accion:''},
  {idTicket: '321531235324', idUsuario: 'U-123125812', categoria: 'DENUNCIA - PRODUCTO', fecha: '22 Febrero', verTicket: 'true', estado:'Pendiente', accion:''},
  {idTicket: '321531235324', idUsuario: 'U-123125812', categoria: 'DENUNCIA - PRODUCTO', fecha: '22 Febrero', verTicket: 'true', estado:'Pendiente', accion:''},
  {idTicket: '321531235324', idUsuario: 'U-123125812', categoria: 'DENUNCIA - PRODUCTO', fecha: '22 Febrero', verTicket: 'true', estado:'Pendiente', accion:''},
  {idTicket: '321531235324', idUsuario: 'U-123125812', categoria: 'DENUNCIA - PRODUCTO', fecha: '22 Febrero', verTicket: 'true', estado:'Pendiente', accion:''},
  {idTicket: '321531235324', idUsuario: 'U-123125812', categoria: 'DENUNCIA - PRODUCTO', fecha: '22 Febrero', verTicket: 'true', estado:'Pendiente', accion:''},
  {idTicket: '321531235324', idUsuario: 'U-123125812', categoria: 'DENUNCIA - PRODUCTO', fecha: '22 Febrero', verTicket: 'true', estado:'Pendiente', accion:''},
  {idTicket: '321531235324', idUsuario: 'U-123125812', categoria: 'DENUNCIA - PRODUCTO', fecha: '22 Febrero', verTicket: 'true', estado:'Pendiente', accion:''},
  {idTicket: '321531235324', idUsuario: 'U-123125812', categoria: 'DENUNCIA - PRODUCTO', fecha: '22 Febrero', verTicket: 'true', estado:'Pendiente', accion:''},
  {idTicket: '321531235324', idUsuario: 'U-123125812', categoria: 'DENUNCIA - PRODUCTO', fecha: '22 Febrero', verTicket: 'true', estado:'Pendiente', accion:''},
  {idTicket: '321531235324', idUsuario: 'U-123125812', categoria: 'DENUNCIA - PRODUCTO', fecha: '22 Febrero', verTicket: 'true', estado:'Pendiente', accion:''},
  {idTicket: '321531235324', idUsuario: 'U-123125812', categoria: 'DENUNCIA - PRODUCTO', fecha: '22 Febrero', verTicket: 'true', estado:'Pendiente', accion:''},
  {idTicket: '321531235324', idUsuario: 'U-123125812', categoria: 'DENUNCIA - PRODUCTO', fecha: '22 Febrero', verTicket: 'true', estado:'Pendiente', accion:''},
  {idTicket: '321531235324', idUsuario: 'U-123125812', categoria: 'DENUNCIA - PRODUCTO', fecha: '22 Febrero', verTicket: 'true', estado:'Pendiente', accion:''},
  {idTicket: '321531235324', idUsuario: 'U-123125812', categoria: 'DENUNCIA - PRODUCTO', fecha: '22 Febrero', verTicket: 'true', estado:'Pendiente', accion:''},
  {idTicket: '321531235324', idUsuario: 'U-123125812', categoria: 'DENUNCIA - PRODUCTO', fecha: '22 Febrero', verTicket: 'true', estado:'Pendiente', accion:''},

];

@Component({
  selector: 'app-search-support',
  templateUrl: './search-support.component.html',
  styleUrls: ['./search-support.component.scss']
})
export class SearchSupportComponent{
  disableSelect = new FormControl(false);

  displayedColumns: string[] = ['idTicket', 'idUsuario', 'categoria', 'fecha', 'verTicket', 'estado', 'accion'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  selection = new SelectionModel<PeriodicElement>(true, []);


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
      return;
    }

    this.selection.select(...this.dataSource.data);
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: PeriodicElement): string {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.idUsuario + 1}`;
  }



  /* Filters */
}
