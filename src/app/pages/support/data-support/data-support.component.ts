import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { DetailComponent } from 'src/app/shared/component/modals/detail/detail.component';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-data-support',
  templateUrl: './data-support.component.html',
  styleUrls: ['./data-support.component.scss']
})
export class DataSupportComponent implements OnInit {

  displayedColumnsPendientes: string[] = ['id', 'fecha', 'categoria', 'estado','accion'];

  dataSourcePendientes = new MatTableDataSource<Pendientes>(ELEMENT_DATA_Pendientes);

  @ViewChild(MatPaginator) paginator:any = MatPaginator;

  isResponse: boolean = true;

  textResponse: String = "";
  valueTextEmpy: boolean = false;

  textState: string = "Pendiente";


  ngAfterViewInit() {
    this.dataSourcePendientes.paginator = this.paginator;
  }

  constructor(
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
  }

  sendResponse(id: string){

    Swal.fire({
      title: 'Estas a punto de enviar esta respuesta.<br> ¿ Deseas continuar ?',
      showDenyButton: false,
      showCancelButton: true,
      confirmButtonText: 'Continuar',
      cancelButtonText: 'Cancelar',
      customClass: {
        container: 'frmConfirm',
      },
    }).then((result:any) => {

      if (result.isConfirmed) {
        let text:any = document.getElementById("txtResponse");

        this.isResponse = !this.isResponse;
        this.textResponse = text.value;
        this.valueTextEmpy = true;

        this.textState = "Resuelto";

      } else if (result.isDenied) {
        this.isResponse = false;
      }

    })
    
  }

  changeFormat(){
    this.isResponse = !this.isResponse;
  }

  openDialog(){
    this.dialog.open(DetailComponent, {
      width: '800px',
      panelClass: 'detailModal',
      data: {
        title: 'Estado del Envio',
      },
    });
  }

}




export interface Pendientes {
  id: string;
  fecha: string;
  categoria: string;
  estado: string;
  accion: string;
}

const ELEMENT_DATA_Pendientes: Pendientes[] = [
  {id: "321531235324", fecha: '23 dic. 2021, 12:30pm', categoria: "DENUNCIA - PEDIDO", estado: "Resuelto",  accion: ''},
  {id: "321531235324", fecha: '23 dic. 2021, 12:30pm', categoria: "DENUNCIA - PEDIDO", estado: "Resuelto",  accion: ''},
  {id: "321531235324", fecha: '23 dic. 2021, 12:30pm', categoria: "DENUNCIA - PEDIDO", estado: "Resuelto",  accion: ''},
  {id: "321531235324", fecha: '23 dic. 2021, 12:30pm', categoria: "DENUNCIA - PEDIDO", estado: "Resuelto",  accion: ''},
  {id: "321531235324", fecha: '23 dic. 2021, 12:30pm', categoria: "DENUNCIA - PEDIDO", estado: "Resuelto",  accion: ''},
  {id: "321531235324", fecha: '23 dic. 2021, 12:30pm', categoria: "DENUNCIA - PEDIDO", estado: "Resuelto",  accion: ''},
  {id: "321531235324", fecha: '23 dic. 2021, 12:30pm', categoria: "DENUNCIA - PEDIDO", estado: "Resuelto",  accion: ''},
  {id: "321531235324", fecha: '23 dic. 2021, 12:30pm', categoria: "DENUNCIA - PEDIDO", estado: "Resuelto",  accion: ''},
  {id: "321531235324", fecha: '23 dic. 2021, 12:30pm', categoria: "DENUNCIA - PEDIDO", estado: "Resuelto",  accion: ''},
];