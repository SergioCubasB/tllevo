import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { EditPageComponent } from 'src/app/shared/component/modals/edit-page/edit-page.component';



export interface PeriodicElement {
  bloque: string;
  fecha: string;
  ver: string;
  accion: string;
}



@Component({
  selector: 'app-documentation',
  templateUrl: './documentation.component.html',
  styleUrls: ['./documentation.component.scss']
})
export class DocumentationComponent implements OnInit {
  displayedColumns: string[] = ['bloque', 'fecha', 'ver', 'accion'];
  dataSource = ELEMENT_DATA;
  constructor(
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
  }

  openDialog(id:string, text:string){
    console.log(text);
    this.dialog.open(EditPageComponent, {
      width: '700px',
      panelClass: 'detailModal',
      data: {
        type: id,
        title: text,
      },
    });
  }

}



const ELEMENT_DATA: PeriodicElement[] = [
  {bloque: "TÉRMINOS Y CONDICIONES", fecha: '24/11/2021', ver: "1", accion: ''},
  {bloque: "POLITICAS DE PRIVACIDAD", fecha: '24/11/2021', ver: "1", accion: ''},

];
