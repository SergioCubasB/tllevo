import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { AddQuestionComponent } from 'src/app/shared/component/modals/add-question/add-question.component';

export interface Preguntas {
  id: number;
  pregunta: string;
  fecha: string;
  categoria: string;
  servicio: string;
  ver: string;
  accion: string;
}

@Component({
  selector: 'app-detail-help',
  templateUrl: './detail-help.component.html',
  styleUrls: ['./detail-help.component.scss']
})
export class DetailHelpComponent implements OnInit {
  displayedColumns: string[] = ['pregunta', 'fecha','ver', 'accion'];
  dataSource = ELEMENT_DATA;

  help_id:any;

  constructor(
    public dialog: MatDialog,
    private actRoute: ActivatedRoute
  ) {
    
   }

  ngOnInit(): void {
    this.getQuestions();
  }

  getQuestions(){
    this.help_id = this.actRoute.snapshot.params['id'];

    let filtered = ELEMENT_DATA.filter( (data)=> {
      return data.id === parseInt(this.help_id) ;
    });
    
    this.dataSource = filtered;
  }

  openDialog(type: any, title:any){
    this.dialog.open(AddQuestionComponent, {
      width: '600px',
      panelClass: 'detailModal',
      data: {
        type: type,
        titulo: title
      },
    });
  }

}

const ELEMENT_DATA: Preguntas[] = [
  {id: 1, pregunta: "Este en un ejemplo de la pregunta1", fecha: '24/12/2021', categoria: "Este en un ejemplo del título", servicio: "Compra", ver: "", accion: ''},
  {id: 1, pregunta: "Este en un ejemplo de la pregunta1", fecha: '24/12/2021', categoria: "Este en un ejemplo del título", servicio: "Compra", ver: "", accion: ''},
  {id: 1, pregunta: "Este en un ejemplo de la pregunta1", fecha: '24/12/2021', categoria: "Este en un ejemplo del título", servicio: "Compra", ver: "", accion: ''},
  {id: 1, pregunta: "Este en un ejemplo de la pregunta1", fecha: '24/12/2021', categoria: "Este en un ejemplo del título", servicio: "Compra", ver: "", accion: ''},
  {id: 1, pregunta: "Este en un ejemplo de la pregunta1", fecha: '24/12/2021', categoria: "Este en un ejemplo del título", servicio: "Compra", ver: "", accion: ''},
  {id: 1, pregunta: "Este en un ejemplo de la pregunta1", fecha: '24/12/2021', categoria: "Este en un ejemplo del título", servicio: "Compra", ver: "", accion: ''},
  {id: 2, pregunta: "Este en un ejemplo de la pregunta2", fecha: '24/12/2021', categoria: "Este en un ejemplo del título", servicio: "Compra", ver: "", accion: ''},
  {id: 3, pregunta: "Este en un ejemplo de la pregunta3", fecha: '24/12/2021', categoria: "Este en un ejemplo del título", servicio: "Compra", ver: "", accion: ''},
  {id: 2, pregunta: "Este en un ejemplo de la pregunta2", fecha: '24/12/2021', categoria: "Este en un ejemplo del título", servicio: "Compra", ver: "", accion: ''},
  {id: 3, pregunta: "Este en un ejemplo de la pregunta3", fecha: '24/12/2021', categoria: "Este en un ejemplo del título", servicio: "Compra", ver: "", accion: ''},

];
