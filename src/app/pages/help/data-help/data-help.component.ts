import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AddCategoryComponent } from 'src/app/shared/component/modals/add-category/add-category.component';
import { ViewQuestionComponent } from 'src/app/shared/component/modals/view-question/view-question.component';

export interface PeriodicElement {
  id: number;
  bloque: string;
  fecha: string;
  ver: string;
  accion: string;
}

@Component({
  selector: 'app-data-help',
  templateUrl: './data-help.component.html',
  styleUrls: ['./data-help.component.scss']
})
export class DataHelpComponent implements OnInit {

  displayedColumns: string[] = ['bloque', 'fecha', 'ver', 'accion'];
  dataSource = ELEMENT_DATA;

  constructor(
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
  }

  openDialog(){
    this.dialog.open(AddCategoryComponent, {
      width: '600px',
      panelClass: 'detailModal',
      data: {
        type: '',
      },
    });
  }

  openView(){
    this.dialog.open(ViewQuestionComponent, {
      width: '600px',
      panelClass: 'detailModal',
      data: {
        type: '',
      },
    });
  }



}

const ELEMENT_DATA: PeriodicElement[] = [
  {id: 1, bloque: "Este en un ejemplo del título para poder acomodar los espacios", fecha: '24/11/2021', ver: "Compra", accion: ''},
  {id: 2, bloque: "Este en un ejemplo del título para poder acomodar los espacios", fecha: '24/11/2021', ver: "Compra", accion: ''},
  {id: 3, bloque: "Este en un ejemplo del título para poder acomodar los espacios", fecha: '24/11/2021', ver: "Compra", accion: ''},
  {id: 4, bloque: "Este en un ejemplo del título para poder acomodar los espacios", fecha: '24/11/2021', ver: "Compra", accion: ''},
  {id: 5, bloque: "Este en un ejemplo del título para poder acomodar los espacios", fecha: '24/11/2021', ver: "Compra", accion: ''},
  {id: 6, bloque: "Este en un ejemplo del título para poder acomodar los espacios", fecha: '24/11/2021', ver: "Venta", accion: ''},
  {id: 7, bloque: "Este en un ejemplo del título para poder acomodar los espacios", fecha: '24/11/2021', ver: "Venta", accion: ''},
  {id: 8, bloque: "Este en un ejemplo del título para poder acomodar los espacios", fecha: '24/11/2021', ver: "Compra", accion: ''},

];