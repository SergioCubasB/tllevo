import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataHelpComponent } from './data-help.component';

describe('DataHelpComponent', () => {
  let component: DataHelpComponent;
  let fixture: ComponentFixture<DataHelpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataHelpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataHelpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
