import {SelectionModel} from '@angular/cdk/collections';

import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import {MatTableDataSource} from '@angular/material/table';

export interface PeriodicElement {
  idUsuario: string;
  correo: string;
  nombre: string;
  ult_ingreso: string;
  f_registro: string;
  accion: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {idUsuario: '124152132', correo: 'jlopez@mentemutante.pe', nombre: 'Junior López', ult_ingreso: '23 dic 2021', f_registro: '14 dic 2021', accion:''},
  {idUsuario: '124152132', correo: 'jlopez@mentemutante.pe', nombre: 'Junior López', ult_ingreso: '23 dic 2021', f_registro: '14 dic 2021', accion:''},
  {idUsuario: '124152132', correo: 'jlopez@mentemutante.pe', nombre: 'Junior López', ult_ingreso: '23 dic 2021', f_registro: '14 dic 2021', accion:''},
  {idUsuario: '124152132', correo: 'jlopez@mentemutante.pe', nombre: 'Junior López', ult_ingreso: '23 dic 2021', f_registro: '14 dic 2021', accion:''},
  {idUsuario: '124152132', correo: 'jlopez@mentemutante.pe', nombre: 'Junior López', ult_ingreso: '23 dic 2021', f_registro: '14 dic 2021', accion:''},
  {idUsuario: '124152132', correo: 'jlopez@mentemutante.pe', nombre: 'Junior López', ult_ingreso: '23 dic 2021', f_registro: '14 dic 2021', accion:''},
  {idUsuario: '124152132', correo: 'jlopez@mentemutante.pe', nombre: 'Junior López', ult_ingreso: '23 dic 2021', f_registro: '14 dic 2021', accion:''},
  {idUsuario: '124152132', correo: 'jlopez@mentemutante.pe', nombre: 'Junior López', ult_ingreso: '23 dic 2021', f_registro: '14 dic 2021', accion:''},
  {idUsuario: '124152132', correo: 'jlopez@mentemutante.pe', nombre: 'Junior López', ult_ingreso: '23 dic 2021', f_registro: '14 dic 2021', accion:''},
  {idUsuario: '124152132', correo: 'jlopez@mentemutante.pe', nombre: 'Junior López', ult_ingreso: '23 dic 2021', f_registro: '14 dic 2021', accion:''},
];

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent{
  disableSelect = new FormControl(false);

  displayedColumns: string[] = ['idUsuario', 'correo', 'nombre', 'ult_ingreso', 'f_registro', 'accion'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  selection = new SelectionModel<PeriodicElement>(true, []);


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
      return;
    }

    this.selection.select(...this.dataSource.data);
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: PeriodicElement): string {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.idUsuario + 1}`;
  }



  /* Filters */
}
