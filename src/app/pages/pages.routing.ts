import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { DocumentationComponent } from "./documentation/documentation.component";
import { DataHelpComponent } from "./help/data-help/data-help.component";
import { DetailHelpComponent } from "./help/detail-help/detail-help.component";
import { HelpComponent } from "./help/help.component";
import { HomeComponent } from "./home/home.component";
import { DataSupportComponent } from "./support/data-support/data-support.component";
import { SearchSupportComponent } from "./support/search-support/search-support.component";
import { SupportComponent } from "./support/support.component";
import { UsersComponent } from "./users/users.component";

const routes: Routes = [
    { path: 'metricas', component: HomeComponent, data: { breadcrumb: 'Metricas'}},
    { path: 'soporte', component: SupportComponent, data: { breadcrumb: 'Soporte'},
        children: [
            { path: '', component: SearchSupportComponent },
            { path: 'ver/:id', component: DataSupportComponent, data: { breadcrumb: 'Ver ticket'}}
        ]
    },
    { path: 'documentacion', component: DocumentationComponent, data: { breadcrumb: 'Documentación'}},
    { path: 'ayuda', component: HelpComponent, data: { breadcrumb: 'Ayuda'},
        children: [
            { path: '', component: DataHelpComponent },
            { path: 'editar/:id', component: DetailHelpComponent, data: { breadcrumb: 'Editar'}}
        ]
    },
    { path: 'usuarios', component: UsersComponent, data: { breadcrumb: 'Usuarios'}}
];


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class PagesRoutingModule {}