export interface Client{
    idClient: String,
    name: String,
    email: String,
    phone: String
}