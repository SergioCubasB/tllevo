import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';


interface DialogData {
  type: '1' | '2' | '3',
  title: 'EDITAR BLOQUE' | 'VER BLOQUE'
}

@Component({
  selector: 'app-view-question',
  templateUrl: './view-question.component.html',
  styleUrls: ['./view-question.component.scss']
})
export class ViewQuestionComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
  }

  closeModal(){
    this.dialog.closeAll();
  }

}


