import { Component, Inject, OnInit } from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';

interface DialogData {
  title: 'panda' | 'unicorn' | 'lion';
  text: 'panda' | 'unicorn' | 'lion';
}
@Component({
  selector: 'app-change-state',
  templateUrl: './change-state.component.html',
  styleUrls: ['./change-state.component.scss']
})
export class ChangeStateComponent implements OnInit {
  favoriteSeason!: string;
  seasons: string[] = ['Preparado', 'En curso', 'Entregado', 'No entregado'];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
  }

  closeModal(){
    this.dialog.closeAll();
  }

}
