import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {
  private history: string[] = [];
  private text: string = "";

  constructor(
    private location: Location
  ) {
   }

  ngOnInit(): void {
    console.log(this.text = "hola");
  }

  goBack() {
    this.location.back();
  }

  goForward(){
    console.log(this.location.forward());
    console.log(this.location.historyGo());

    this.location.forward();
  }

}
