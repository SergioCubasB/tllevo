import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { MaterialModule } from "./material/material.module";
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { HeaderComponent } from "./component/header/header.component";
import { FooterComponent } from "./component/footer/footer.component";
import { AppRoutingModule } from "../app-routing.module";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { TopbarComponent } from './component/topbar/topbar.component';
import { InternalNavigationComponent } from './component/internal-navigation/internal-navigation.component';
import { NavbarComponent } from './component/navbar/navbar.component';
import { DetailComponent } from "./component/modals/detail/detail.component";
import { ChangeStateComponent } from "./component/modals/change-state/change-state.component";
import { EditPageComponent } from './component/modals/edit-page/edit-page.component';
import { AddCategoryComponent } from './component/modals/add-category/add-category.component';
import { BreadcrumbComponent } from './component/breadcrumb/breadcrumb.component';
import { AddQuestionComponent } from './component/modals/add-question/add-question.component';
import { ViewQuestionComponent } from './component/modals/view-question/view-question.component';

import {BreadcrumbModule} from 'angular-crumbs';

@NgModule({
    declarations: [
        HeaderComponent,
        FooterComponent,
        TopbarComponent,
        InternalNavigationComponent,
        NavbarComponent,
        DetailComponent,
        ChangeStateComponent,
        EditPageComponent,
        AddCategoryComponent,
        BreadcrumbComponent,
        AddQuestionComponent,
        ViewQuestionComponent
    ],
    imports: [
        BrowserAnimationsModule,
        ReactiveFormsModule,
        BrowserModule,
        FormsModule,      
        
        MaterialModule,
        AppRoutingModule,
        BreadcrumbModule
    ],
    exports:[
        MaterialModule,

        HeaderComponent,
        FooterComponent,
        NavbarComponent,
        InternalNavigationComponent,
        ChangeStateComponent,
        BreadcrumbComponent,
        ViewQuestionComponent
    ]
})
export class SharedModule{}