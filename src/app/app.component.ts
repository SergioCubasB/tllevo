import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'tllevo';
  isLogin!: boolean;

  constructor(
    private location: Location
  ) { }

  ngOnInit(): void {
    this.location.path() === '/login' ? this.isLogin = false : this.isLogin = true;
  }

  
}
