import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InternalNavigationComponent } from './internal-navigation.component';

describe('InternalNavigationComponent', () => {
  let component: InternalNavigationComponent;
  let fixture: ComponentFixture<InternalNavigationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InternalNavigationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InternalNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
